package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    // GET http://localhost:9000/api/v1/clientes -> List<Cliente> obtenerClientes()
    @GetMapping
    public List<Cliente> obtenerClientes() {
        return this.servicioCliente.obtenerClientes();
    }

    // POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    @PostMapping
    public void agregarCliente(@RequestBody Cliente cliente) {
        this.servicioCliente.insertarClienteNuevo(cliente);
    }

    // GET http://localhost:9000/api/v1/clientes/{documento} -> obtenerUnCliente(documento)
    // GET http://localhost:9000/api/v1/clientes/12345678 -> obtenerUnCliente("12345678")
    @GetMapping("/{documento}")
    public Cliente obtenerUnCliente(@PathVariable String documento) {
        return this.servicioCliente.obtenerCliente(documento);
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> reemplazarUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> reemplazarUnCliente("12345678", DATOS)
    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        this.servicioCliente.guardarCliente(cliente);
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{documento}")
    public void emparacharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
    }
}
