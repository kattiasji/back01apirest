package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;

import java.util.List;

public interface ServicioCliente {

    // CRUD

    public List<Cliente> obtenerClientes();

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // READ
    public Cliente obtenerCliente(String documento);

    // UPDATE (solamente modificar, no crear).
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);

    // DELETE
    public void borrarCliente(String documento);
}
