package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    public final Map<String,Cliente> clientes = new ConcurrentHashMap<String,Cliente>();

    @Override
    public List<Cliente> obtenerClientes() {
        return List.copyOf(this.clientes.values());
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento, cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        return this.clientes.get(documento);
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        this.clientes.replace(cliente.documento, cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente = this.clientes.get(parche.documento);

        if(parche.edad != existente.edad)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientes.replace(existente.documento, existente);
    }

    @Override
    public void borrarCliente(String documento) {
        this.clientes.remove(documento);
    }
}
